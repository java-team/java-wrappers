# jvm-list.sh: a list of available JVM and some shortcuts
# Copyright 2008, 2009 by Vincent Fourmond <fourmond@debian.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# All variables defined here can be used as arguments for the
# find_java_runtime function if you strip the __jvm_ at the beginning
# of the variable...
#
# They can also be used for the JAVA_FLAVOR environment variable, see
# java-wrappers(7).

__arch=$(dpkg --print-architecture)

# default runtime
__jvm_default="/usr/lib/jvm/default-java"

# Oracle runtimes
__jvm_oracle8="/usr/lib/jvm/java-8-oracle /usr/lib/jvm/jdk-8-oracle-* /usr/lib/jvm/jre-8-oracle-*"
__jvm_oracle11="/usr/lib/jvm/java-11-oracle /usr/lib/jvm/jdk-11-oracle-* /usr/lib/jvm/jre-11-oracle-*"
__jvm_oracle17="/usr/lib/jvm/java-17-oracle /usr/lib/jvm/jdk-17-oracle-* /usr/lib/jvm/jre-17-oracle-*"
__jvm_oracle21="/usr/lib/jvm/java-21-oracle /usr/lib/jvm/jdk-21-oracle-* /usr/lib/jvm/jre-21-oracle-*"
__jvm_oracle22="/usr/lib/jvm/java-22-oracle /usr/lib/jvm/jdk-22-oracle-* /usr/lib/jvm/jre-22-oracle-*"
__jvm_oracle23="/usr/lib/jvm/java-23-oracle /usr/lib/jvm/jdk-23-oracle-* /usr/lib/jvm/jre-23-oracle-*"
__jvm_oracle24="/usr/lib/jvm/java-24-oracle /usr/lib/jvm/jdk-24-oracle-* /usr/lib/jvm/jre-24-oracle-*"
__jvm_oracle25="/usr/lib/jvm/java-25-oracle /usr/lib/jvm/jdk-25-oracle-* /usr/lib/jvm/jre-25-oracle-*"

# Now, free runtimes:
__jvm_openjdk8="/usr/lib/jvm/java-8-openjdk-$__arch /usr/lib/jvm/java-8-openjdk"
__jvm_openjdk11="/usr/lib/jvm/java-11-openjdk-$__arch /usr/lib/jvm/java-11-openjdk"
__jvm_openjdk17="/usr/lib/jvm/java-17-openjdk-$__arch /usr/lib/jvm/java-17-openjdk"
__jvm_openjdk21="/usr/lib/jvm/java-21-openjdk-$__arch /usr/lib/jvm/java-21-openjdk"
__jvm_openjdk22="/usr/lib/jvm/java-22-openjdk-$__arch /usr/lib/jvm/java-22-openjdk"
__jvm_openjdk23="/usr/lib/jvm/java-23-openjdk-$__arch /usr/lib/jvm/java-23-openjdk"
__jvm_openjdk24="/usr/lib/jvm/java-24-openjdk-$__arch /usr/lib/jvm/java-24-openjdk"
__jvm_openjdk25="/usr/lib/jvm/java-25-openjdk-$__arch /usr/lib/jvm/java-25-openjdk"


# And a few aliases
__jvm_openjdk="$__jvm_openjdk25 $__jvm_openjdk24 $__jvm_openjdk23 $__jvm_openjdk22 $__jvm_openjdk21 $__jvm_openjdk17 $__jvm_openjdk11 $__jvm_openjdk8"

# The java* runtimes:
__jvm_java25="$__jvm_openjdk25 $__jvm_oracle25"
__jvm_java24="$__jvm_java25 $__jvm_openjdk24 $__jvm_oracle24"
__jvm_java23="$__jvm_java24 $__jvm_openjdk23 $__jvm_oracle23"
__jvm_java22="$__jvm_java23 $__jvm_openjdk22 $__jvm_oracle22"
__jvm_java21="$__jvm_java22 $__jvm_openjdk21 $__jvm_oracle21"
__jvm_java17="$__jvm_java21 $__jvm_openjdk17 $__jvm_oracle17"
__jvm_java11="$__jvm_java17 $__jvm_openjdk11 $__jvm_oracle11"
__jvm_java8="$__jvm_java11 $__jvm_openjdk8 $__jvm_oracle8"
__jvm_java7="$__jvm_java8"
# -> corresponds to Provides: java6-runtime
__jvm_java6="$__jvm_java7"
# -> corresponds to Provides: java5-runtime
__jvm_java5="$__jvm_java6"
# -> corresponds to Provides: java2-runtime
__jvm_java2="$__jvm_java5"

# current java alternatives
__jvm_alt=$(readlink /etc/alternatives/java|sed -n 's!\(.*\)/bin/[^/]*$!\1!p')

# All JVMs
__jvm_all="$__jvm_default /usr/lib/jvm/*"

# Probably here should come a few meaningful global aliases.
